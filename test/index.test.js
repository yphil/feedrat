var should  = require('should'),
    feedrat = require(__dirname + '/../');

describe('feedrat', function() {

  it('reads a feed URL OK', function(done) {
    feedrat('http://www.lemonde.fr/rss/une.xml', function(err, url) {
      if (err) return done(err);
      url.should.eql('http://www.lemonde.fr/rss/une.xml');
      done();
    });
  });

  it('handles a website that has blocked us', function(done) {
    feedrat('http://www.leparisien.fr/', function(err, url) {
      if (err) return done(err);
      url.should.eql('http://www.leparisien.fr/');
      url.should.not.exist();

      done();
    });
  });

  it('discovers the RSS feed by reading the link rel', function(done) {
    feedrat('http://lemonde.fr/', function(err, url) {
      if (err) return done(err);
      url.should.eql('http://www.lemonde.fr/rss/une.xml');
      done();
    });
  });

  it('handles finding several RSS feeds', function(done) {
    feedrat('http://www.disclose.tv/', function(err, url) {
      if (err) return done(err);
      url.should.be.oneOf('http://www.disclose.tv/rss/posts', 'http://www.disclose.tv/rss/videos');
      done();
    });
  });

  it('discovers a feed found from a <a> tag', function(done) {
    feedrat('http://www.lefigaro.fr', function(err, url) {
      if (err) return done(err);
      url.should.eql('http://www.lefigaro.fr/rss/figaro_flash-actu.xml');
      done();
    });
  });

  it('can handle https protocol', function(done) {
    feedrat('https://lwn.net/', function(err, url) {
      if (err) return done(err);
      url.should.eql('https://lwn.net/headlines/newrss');
      done();
    });
  });

  it('builds a YTube URL from the channels', function(done) {
    feedrat('https://www.youtube.com/channel/UCeUNM9NqJqZXfRNeuW4_2sg', function(err, url) {
      if (err) return done(err);
      url.should.eql('https://www.youtube.com/feeds/videos.xml?channel_id=UCeUNM9NqJqZXfRNeuW4_2sg');
      done();
    });
  });

  it('builds a search feed from a non-URL string', function(done) {
    feedrat('bitcoin crash', function(err, url) {
      if (err) return done(err);
      url.should.eql('https://trouvons.org/?format=rss&categories=news&q=bitcoin,crash');
      done();
    });
  });

  it('can find a bunch of feeds', function(done) {
    feedrat('http://www.futura-sciences.com', function(err, url) {
      if (err) return done(err);
      url.should.be.oneOf('https://www.futura-sciences.com/rss/photos.xml', 'https://www.futura-sciences.com/rss/actualites.xml');
      done();
    });
  });
});
