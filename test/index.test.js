const should  = require('should'),
      feedrat = require(__dirname + '/../');

describe('feedrat', function() {
  
  it('validates an existing feed URL as-is', function(done) {
    feedrat('http://www.lemonde.fr/rss/une.xml', function(err, url) {
      if (err) return done(err);
      url[0].should.eql('http://www.lemonde.fr/rss/une.xml');
      return done();
    });
  });

  it('discovers an RSS feed by reading the link rel', function(done) {
    feedrat('http://lemonde.fr/', function(err, url) {
      if (err) return done(err);
      url[0].should.eql('https://www.lemonde.fr/rss/une.xml');
      return done();
    });
  });

  it('finds a lot of feed URLS', function(done) {
    feedrat('https://randomsblogs.blogspot.com/', function(err, url) {
      if (err) return done(err);
      url.length.should.eql(3);
      return done();
    });
  });

  it('finds a feed from the usual suspects list relative to the TLD', function(done) {
    feedrat('https://news.cnrs.fr/', function(err, url) {
      if (err) return done(err);
      url[0].should.be.oneOf('https://news.cnrs.fr/rss.xml', 'https://news.cnrs.fr/rss');
      return done();
    });
  });

  it('finds a feed from the DOM even when there is one on the TLD', function(done) {
    feedrat('https://yphil.bitbucket.io/', function(err, url) {
      if (err) return done(err);
      url[0].should.eql('https://yphil.bitbucket.io/rss/feedone.xml');
      return done();
    });
  });

  it('finds atom/xml rel links', function(done) {
    feedrat('https://sima78.chispa.fr/', function(err, url) {
      if (err) return done(err);
      url[0].should.eql('https://sima78.chispa.fr/index.php?feed/atom');
      return done();
    });
  });

  it('does not trip on openSearch xml rel links', function(done) {
    feedrat('https://apnews.com/', function(err) {
      if (err) return done();      
      return done();
    });
  });

  it('handles SSL certificate errors', function(done) {
    feedrat('https://test.lsdm.live/backend.php', function(err) {
      if (err) return done();
      return done();
    });
  });

  it('returns an RSS feed URL made of a query string', function(done) {
    feedrat('https://www.acrimed.org', function(err, url) {
      if (err) return done(err);
      url[0].should.eql('https://www.acrimed.org/spip.php?page=backend');
      return done();
    });
  });

  it('finds a feed from the usual suspects list relative to the URI', function(done) {
    feedrat('https://www.reddit.com/r/news', function(err, url) {
      if (err) return done(err);
      url[0].should.eql('https://www.reddit.com/r/news.rss');
      return done();
    });
  });

  it('builds a feed URL from a YouTube channel URL', function(done) {
    feedrat('https://www.youtube.com/channel/UCeUNM9NqJqZXfRNeuW4_2sg', function(err, url) {
      if (err) return done(err);
      url[0].should.eql('https://www.youtube.com/feeds/videos.xml?channel_id=UCeUNM9NqJqZXfRNeuW4_2sg');
      return done();
    });
  });

  it('can handle https URLs', function(done) {
    feedrat('https://lwn.net/', function(err, url) {
      if (err) return done(err);
      url[0].should.eql('https://lwn.net/headlines/newrss');
      return done();
    });
  });

  it('does not return a false positive', function(done) {
    feedrat('https://www.cracked.com', function(err) {
      if (err) return done();
      return done();
    });
  });

});
