# Feedrat

Try and discover a website's RSS/Atom feed, using various strategies.

## Installation

`npm install feedrat`

## Usage

```javascript
var feedrat = require('feedrat');

feedrat(url, function(err, feed) {

    if (feed) {
        res.send(feed)
    } else {
        res.status(500).send('No feed found')
    }

});

```

Official repo [here](https://bitbucket.org/yphil/feedrat) ♫ Brought to you by [yPhil](http://yphil.bitbucket.io/) ♥ Consider [helping](https://liberapay.com/yPhil/donate)
