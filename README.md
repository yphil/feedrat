# Feedrat

*Search, find and report a web page's RSS feed(s)*

---

[![pipeline status](https://framagit.org/yphil/feedrat/badges/master/pipeline.svg)](https://framagit.org/yphil/feedrat/-/pipelines)
[![coverage](https://framagit.org/yphil/feedrat/badges/master/coverage.svg)](https://framagit.org/yphil/feedrat/-/pipelines)
[![website](https://img.shields.io/website?down_message=down&up_color=brightgreen&up_message=up&url=https%3A%2F%2Fpetrolette.space)](https://petrolette.space)
[![Liberapay](https://img.shields.io/liberapay/receives/yPhil?logo=liberapay)](https://liberapay.com/yPhil/donate)
[![Liberapay](https://img.shields.io/liberapay/goal/yPhil?logo=liberapay)](https://liberapay.com/yPhil/donate)
[![Ko-Fi](https://img.shields.io/badge/Ko--fi-F16061?label=buy+me+a&style=flat&logo=ko-fi&logoColor=white)](https://ko-fi.com/yphil/tiers)

## Features

Searches for XML feeds in a given URI using two strategies: usual suspects (url/feed, url/rss.xml, etc.) and DOM scraping in two places : The very URI, and then the domain thereof.

1. Parse the DOM of the URI for a link rel
2. Parse the DOM of root of the TLD for a link rel
3. Check the URI for usual suspects
4. Check the root of the TLD for usual suspects

And returns an array containing the URLs found.

## Installation

`npm install feedrat`

## Testing

`npm test`

## Usage

```javascript

const feedrat = require('feedrat');

feedrat('https://www.reddit.com/r/news', function(err, feeds) {

    if (feeds) {
        res.send(feeds)
    } else {
        res.status(500).send('No feed found')
    }

});

```

Brought to you ♫ by [yPhil](http://yphil.bitbucket.io/) Please consider ♥ [helping](https://liberapay.com/yPhil/donate) icon by Mattahan (Paul Davey)
