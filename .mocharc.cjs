const {colors, symbols} = require('mocha/lib/reporters/base');

colors.pass = 32;
symbols.ok = '😀';
symbols.err = '💀';