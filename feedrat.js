const fetch = require('node-fetch'),
      cheerio = require('cheerio');

module.exports = function(url, callback) {

  try {
    var p = new URL(url);
  } catch (err) {
    return callback('Not a valid URL');
  } 
  
  const protocol = p.protocol != null ? p.protocol : 'http:',
        tld = protocol + '//' + p.host,
        types = ['application/rss+xml', 'application/atom+xml'],
        usualSuspects = ['/feed.xml', '/rss.xml', '/feed', '/rss', '/atom.xml', '.rss'];
  
  let feed  = url.replace(/\/$/, '');
  let feeds = [];

  async function isRss(u) {
    try {
      const response = await fetch(u);
      return response.ok && response.headers.get('content-type').includes('xml');
    } catch (err) {
      return false;
    }
  }

  async function checkSuspects(f) {
    for (const suspect of usualSuspects) {
      if (await isRss(f + suspect)) {
        feeds.push(f + suspect)
        return feeds
      };
    }
    return '';
  }

  async function checkTheDom(u) {
    const response = await fetch(u);
    const $ = cheerio.load(await response.text());
    if ($('link') && $('link').length > 0) {
      $('link').each(function() {
        if ($(this).attr('type') && types.some(type => $(this).attr('type').includes(type))) {
          if (!$(this).attr('href').startsWith('http')) {
            feed = ($(this).attr('href').startsWith('/')) ? tld + ($(this).attr('href')) : tld + '/' + ($(this).attr('href'));
          } else {
            feed = $(this).attr('href');
          }
            feeds.push(feed);
        }
      });
      
      return feeds;
    } else {
      return '';
    }
  }

  async function checkAll() {

    if (await isRss(feed)) {
      feeds.push(feed)
      return feeds;
    }
    
    let res = await checkTheDom(feed);
    if (res.length) return res;

    res = await checkSuspects(feed);
    if (res.length) return res;

    res = await checkTheDom(tld);
    if (res.length) return res;

    res = await checkSuspects(tld);
    if (res.length) return res;

    return false;
    
  }
  
  (async function() {
    
    try {
      let res =  await checkAll();
      return (res) ? callback(null, res) : callback('No feed found') 
    } catch (err) {
      return callback(err);
    }

  })();

};
