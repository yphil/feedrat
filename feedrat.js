const request = require('request'),
      cheerio = require('cheerio'),
      Url     = require('url');

module.exports = function(url, callback) {
  var p    = Url.parse(url),
      protocol = p.protocol != null ? p.protocol : 'http:',
      root = protocol + "//" + p.hostname,
      feed  = url,
      isUrl = new RegExp('^(?:[a-z]+:)?//', 'i'),
      searchPrefix = 'https://trouvons.org/?format=rss&categories=news&q=',
      videoPrefix = 'https://www.youtube.com/feeds/videos.xml?channel_id=',
      linkTypes = ['application/atom+xml',
                   'application/rss+xml',
                   'application/xhtml+xml'],
      usualSuspects = [url,
                       p.hostname + '/rss',
                       p.hostname + '/feed',
                       p.hostname + '/atom',
                       p.hostname + '/rss.xml',
                       p.hostname + '.rss',
                       p.hostname + './rss',
                       p.hostname + '/atom.xml',
                       p.hostname + '/feed.xml'];

  if (!p.protocol) return callback(null, searchPrefix + url.split(' '));

  if (p.host.endsWith('youtube.com') && p.path.startsWith('/channel') && p.path.split('/').length === 3) {
    return callback(null, videoPrefix + p.path.split('/')[2]);
  }

  does_it_render(url, function(err, renders) {
    if (renders) {
      console.log('url!');
      return callback(null, url);
    }
  });

  request(feed, function(err, res, body) {

    const $ = cheerio.load(body);

    console.log('req! (%s) (%s)', $('title').text(), err, res.statusCode);

    if (res.statusCode > 400) {
      return callback(null, null);
    }


    if ($('link').length > 0) {
      console.log('link!');
      $('link').each(function() {
        if ($(this).attr('type'), linkTypes.indexOf($(this).attr('type')) > -1) {
          feed = $(this).attr('href');
          if (!isUrl.test(feed)) feed =  root + feed;
          return callback(null, feed);
        }
      });
    }

    if ($('a[href$=".xml"]').length > 0) {
      return callback(null, $('a[href$=".xml"]').attr('href'));
    }

    if ($('a[href$=".xml"]').length === 0 && $('link').length === 0) {
      console.log('obv!');
      // Search obvious places
      for (let i = 0; i < usualSuspects.length; i++) {
        does_it_render(usualSuspects[i], function(err, renders) {
          if (renders) {
            return callback(null, usualSuspects[i]);
          }
        });
      }
    }

  });

};

function does_it_render(url, callback) {
  request(url, function(err, res, body) {
    if (err || !body || body === '' || !res.headers['content-type'].includes('xml')) {
      return callback(err);
    }
    callback(null, res.statusCode === 200);
  });
}
